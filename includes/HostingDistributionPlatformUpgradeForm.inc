<?php
/**
 * @file The HostingDistributionPlatformUpgradeForm class.
 */

class HostingDistributionPlatformUpgradeForm extends HostingForm {

  // The HostingDistributionPlatform associated with the form.
  private $platform = FALSE;

  public function __construct(&$form, &$form_state, $node = FALSE) {
    parent::__construct($form, $form_state, $node);
    if ($this->node) {
      $this->platform = new HostingDistributionPlatform($this->node);
      $this->log = $this->platform->getDistribution()->log;
    }
  }

  /**
   * Page callback for the platform upgrade confirmation form.
   */
  function build() {
    $step = isset($this->form_state['storage']['step']) ? $this->form_state['storage']['step'] : 1;

    // Step 1 - Check sites
    // TODO: Figure out how to trigger the batch job directly, rather than going
    // through this redundant confirmation form. For example, when we follow
    // the 'Go back' link from a site upgrade comparison, we have to click
    // "Check" again.
    if ($step == 1) {
      $this->form['description'] = array(
        '#type' => 'item',
        '#description' => 'Check this platform\'s sites for upgrades',
      );
      $this->form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Check'),
      );
    }

    // Step 2 - review sites that pass or fail the requirements to be upgraded
    if ($step == 2) {
      $title = array(
        'passed' => t("The following sites will be upgraded"),
        'failed' => t("The following sites will not be upgraded"),
      );
      $header = array(t('Site'), t('Upgrades'), t('Warnings'), t('Errors'), t('Actions'));
      $options['attributes']['class'] = 'hosting-package-comparison-link';
      foreach (array('passed', 'failed') as $type) {
        if (isset($this->form_state['storage'][$type]) && sizeof($this->form_state['storage'][$type])) {

          $rows = array();

          foreach ($this->form_state['storage'][$type] as $site_id => $url) {
            $this->form['output'][$type]['title'] = array(
              '#type' => 'markup',
              '#markup' => '<h2>' . $title[$type] . '</h2>',
            );
            $status = $this->form_state['storage']['status'][$site_id];
            $row = array(array(
                'data' => $url,
                'class' => array('hosting-status'),
              ), $status['upgrade'], $status['missing'], $status['error']);
            if (isset($this->form_state['storage']['instance'][$site_id])) {
              $site = new HostingDistributionSite(node_load($site_id));
              $profile_instance = $site->getTargetProfilePackageInstance();
              $link = l(t('Compare'), 'hosting_confirm/' . arg(1) . '/'. arg(2) . '/compare/' . $site->node->nid . '/' . $profile_instance->iid, $options);
            }
            else {
              $link = t('Profile not found');
            }
            $row[] = $link;
            $rows[] = array(
              'data' => $row,
              'class' => array('hosting-' . _hosting_migrate_site_list_class($status)),
            );
          }
          $this->form['output'][$type]['table'] = array(
            '#type' => 'markup',
            '#markup' => theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('class' => array('hosting-table')))),
          );

        }
      }
      if (sizeof($this->form_state['storage']['passed'])) {
        $this->form['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Confirm'),
        );
      }
    }
    // Step 3 - Done.
    if ($step == 3) {
      return $this->closeOverlay();
    }

    $this->form['cancel'] = array(
      '#type' => 'submit',
      '#value' => t('Cancel'),
    );

    return $this->form;
  }

  /**
   * Submit handler for platform upgrades.
   */
  function submit() {
    if ($this->form_state['input']['op'] == 'Cancel') {
      return $this->closeOverlay();
    }

    $step = isset($this->form_state['storage']['step']) ? $this->form_state['storage']['step'] : 1;
    switch ($step) {
      case 1: // Check upgrade status of the platform's sites.
        $target = $this->platform->getUpgradeTarget();
        drupal_add_js(drupal_get_path('module', 'hosting_migrate') . '/hosting_migrate.js');
        $this->form_state['storage']['current_platform'] = $this->node->nid;
        $this->form_state['storage']['target_platform'] = $target;
        $max_per_batch = 5;

        $result = db_query("SELECT n.nid, n.title FROM  {hosting_site} s LEFT JOIN {node} n ON n.nid=s.nid WHERE platform = :platform AND s.status = :status and s.verified > :verified ORDER BY n.title", array(':platform' => $this->node->nid, ':status' => HOSTING_SITE_ENABLED, ':verified' => 0));
        $operations = array();
        while ($site = $result->fetch()) {
          $operations[] = array(
            'hosting_migrate_platform_batch',
            array($site->nid, $target, $this->form_state),
          );
        }
        if (sizeof($operations)) {
          $batch = array(
            'operations' => $operations,
            'title' => t('Checking for sites that can be upgraded.'),
            'init_message' => t('Retrieving list of sites.'),
            'progress_message' => t('Evaluated @current out of @total sites.'),
            'error_message' => t('Platform upgrade has encountered an error.'),
            'file' => drupal_get_path('module', 'hosting_migrate') . '/hosting_migrate.batch.inc',
          );
          batch_set($batch);
          if (!empty($GLOBALS['modalframe_page_template'])) {
            $batch = &batch_get();
            $batch['url'] = 'hosting/js/batch';
            $batch['source_page'] = 'hosting/js/' . $_GET['q'];
          }
        }
        break;
      case 2: // Enqueue migration tasks for each of the platform's sites.
        $current = $this->form_state['storage']['current_platform'];
        $target = $this->form_state['storage']['target_platform'];

        // Create a task node for logging purposes.
        $this->log->setTaskType('upgrade');
        $this->log->setNID($this->node->nid);
        $target_node = node_load($target);
        $this->log->info(t('Scheduling site migrations to :target.', array(':target' => $target_node->title)));

        $operations = array();
        foreach ($this->form_state['storage']['passed'] as $nid => $url) {
          $operations[] = array(
            'hosting_migrate_platform_submit_batch',
            array($nid, $this->form_state['storage']['target_platform'], $url, $this->log->getTask()->vid),
          );
        }

        if (sizeof($operations)) {
          $batch = array(
            'operations' => $operations,
            'finished' => 'hosting_distribution_platform_submit_finished',
            'title' => t('Submitting sites for upgrade.'),
            'init_message' => t('Retrieving list of sites.'),
            'progress_message' => t('Submitted @current out of @total sites.'),
            'error_message' => t('Bulk upgrade has encountered an error.'),
            'file' => drupal_get_path('module', 'hosting_migrate') . '/hosting_migrate.batch.inc',
          );
          batch_set($batch);

          if (!empty($GLOBALS['modalframe_page_template'])) {
            $batch = &batch_get();
            $batch['url'] = 'hosting/js/batch';
            $batch['source_page'] = 'hosting/js/' . $_GET['q'];
          }
        }
        break;
    }
    $this->form_state['rebuild'] = TRUE;
    $this->form_state['storage']['step'] = $step + 1;
  }

  /**
   * Batch finished callback for platform upgrade submission.
   */
  function finish() {
    drupal_set_message(t('The sites have been added to the task queue to be upgraded'));
  }

}
