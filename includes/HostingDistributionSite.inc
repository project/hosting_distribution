<?php
/**
 * @file The HostingDistributionSite class.
 */

class HostingDistributionSite extends HostingNode {

  // The site's platform.
  private $platform = FALSE;

  // The site's distribution.
  private $distribution = FALSE;

  // The site's upgrade target.
  private $upgrade_target = FALSE;

  public function getPlatform() {
    if (!$this->platform) {
      $platform = node_load($this->node->platform);
      $this->platform = new HostingDistributionPlatform($platform);
    }
    return $this->platform;
  }

  public function getDistribution() {
    if (!$this->distribution) {
      $this->distribution = $this->getPlatform()->getDistribution();
    }
    return $this->distribution;
  }

  public function getUpgradeTarget() {
    if (!$this->upgrade_target) {
      $this->upgrade_target = $this->getPlatform()->getUpgradeTarget();
    }
    return $this->upgrade_target;
  }

  /**
   * Access callback to ensure an upgrade target is specified, the site is
   * enabled, and user has appropriate permission.
   */
  public function taskAccess() {
    $target = $this->getUpgradeTarget();

    if (!$target || $target === $this->node->platform) {
      return FALSE;
    }
    return $this->node->status && user_access("administer sites") || user_access("create site upgrade task");
  }

  /**
   * Return the profile package from a site's upgrade target.
   */
  function getTargetProfilePackageInstance() {
    $target = $this->getUpgradeTarget();

    $this->node->check_profile_migrations = TRUE;
    $valid_options = hosting_site_available_options($this->node, $target);

    if (in_array($target, $valid_options['platform'])) {
      return _hosting_package_instances_load(
        array(
          'r.type' => 'platform',
          'n.nid' => $this->node->profile,
          'h.status' => HOSTING_PLATFORM_ENABLED,
          'h.nid' => $target,
        ), FALSE
      );
    }
    else {
      return FALSE;
    }
  }

}
