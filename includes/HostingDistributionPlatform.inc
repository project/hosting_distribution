<?php
/**
 * @file The HostingDistributionPlatform class.
 */

class HostingDistributionPlatform extends HostingNode {

  protected $info_fields = ['field_distribution'];

  // The platform's distribution.
  private $distribution = FALSE;

  // The platform's upgrade target.
  private $upgrade_target = FALSE;

  public function getDistribution() {
    if (!$this->distribution) {
      $node = node_load($this->getEntityReference('field_distribution'));
      $this->distribution = new HostingDistribution($node);
    }
    return $this->distribution;
  }

  public function getUpgradeTarget() {
    if (!$this->upgrade_target) {
      $this->upgrade_target = $this->getDistribution()->getUpgradeTarget();
    }
    return $this->upgrade_target;
  }

  /**
   * Access callback to ensure an upgrade target is specified, the site or
   * platform is enabled, and user has appropriate permission.
   */
  public function taskAccess() {
    $target = $this->getUpgradeTarget();

    $site_count = hosting_site_count($this->node->nid);

    if (!$target || $target === $this->node->nid || !$site_count) {
      return FALSE;
    }
    return $this->node->status && user_access("administer platforms") || user_access("create platform upgrade task");
  }

}
