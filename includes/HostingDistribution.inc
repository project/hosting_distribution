<?php
/**
 * @file The HostingDistribution class.
 */

class HostingDistribution extends HostingNode {

  // Fields to format in line with Aegir standards.
  protected $info_fields = ['field_upgrade_target'];

  // The distribution's upgrade target.
  protected $upgrade_target = FALSE;

  function __construct($node) {
    parent::__construct($node);
    $this->log = new HostingDistributionLog($this);
  }

  /**
   * Return the specified upgrade target (platform) for this distribution.
   */
  public function getUpgradeTarget() {
    if (!$this->upgrade_target) {
      $this->upgrade_target = $this->getEntityReference('field_upgrade_target');
    }
    return $this->upgrade_target;
  }

  /**
   * Return distribution-related menu items.
   */
  static function getMenuItems() {
    $items = array();

    $items['admin/hosting/distributions'] = array(
      'title' => t('Distributions'),
      'description' => 'Configure settings related to distributions.',
      'page arguments' => array('hosting_distribution_settings_form'),
      'access arguments' => array('administer distribution settings'),
      'tab_parent' => 'admin/hosting',
      'type' => MENU_LOCAL_TASK,
    );

    $items['hosting/distributions/list'] = array(
      'title' => t('List'),
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
    );
    $items['hosting/distributions/add'] = array(
      'title' => t('Add distribution'),
      'type' => MENU_LOCAL_TASK,
      'page callback' => 'drupal_goto',
      'page arguments' => array('node/add/distribution'),
      'access callback' => 'node_access',
      'access arguments' => array('create', 'distribution'),
    );
    $items['hosting_confirm/%/%/compare'] = array(
      'title' => t('Compare package upgrades'),
      'page callback' => 'hosting_distribution_upgrade_comparison',
      'access callback' => 'hosting_distribution_task_access',
      'access arguments' => array(1),
      'type' => MENU_CALLBACK,
    );
    return $items;
  }

  /**
   * Define user permissions.
   */
  static function getPermissions() {
    $permissions = HostingDistributionTask::getPermissions();

    $permissions['administer distribution settings'] = array(
      'title' => t('Administer distribution settings'),
    );
    $permissions['view distribution list'] = array(
      'title' => t('View the list of distributions'),
    );

    return $permissions;
  }

  public function settingsForm() {
    $form = array();

    $form['automatic_distribution_platform_lock'] = array(
      '#type' => 'checkbox',
      '#title' => t('Handle platform locking.'),
      '#description' => t('Automatically lock/unlock distribution platforms as appropriate.'),
      '#default_value' => self::getAutomaticPlatformLockSetting(),
    );

    return system_settings_form($form);
  }

  /**
   * Return a list of NIDs for all platforms under this distribution.
   */
  public function getPlatforms() {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'platform')
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldCondition('field_distribution', 'target_id', $this->getNid());

    $result = $query->execute();
    if (isset($result['node'])) {
      return array_combine(array_keys($result['node']), array_keys($result['node']));
    }
    else {
      return array();
    }
  }

  /**
   * Return a list of platforms under this distribution, except the upgrade
   * target.
   */
  public function getOutOfDatePlatforms() {
    $platforms = $this->getPlatforms();
    unset($platforms[$this->getUpgradeTarget()]);
    return $platforms;
  }

  /**
   * Return a list of out-of-date platforms to lock.
   */
  public function getPlatformsToLock() {
    $platforms = node_load_multiple($this->getOutOfDatePlatforms());
    foreach ($platforms as $nid => $node) {
      if ($node->platform_status != HOSTING_PLATFORM_ENABLED) {
        unset($platforms[$nid]);
      }
      else {
        $platforms[$nid] = $nid;
      }
    }
    return $platforms;
  }

  public function lockOutOfDatePlatforms() {
    if ($platforms = $this->getPlatformsToLock()) {
      drupal_set_message('Locking out-of-date distribution platforms.');
      foreach ($this->getPlatformsToLock() as $nid) {
        hosting_add_task($nid, 'lock');
      }
    }
  }

  protected function getOriginalUpgradeTarget() {
    $original = $this->node->original;
    return $original->field_upgrade_target[$original->language][0]['target_id'];
  }

  public function unlockUpgradeTarget() {
    if ($this->getUpgradeTarget() != $this->getOriginalUpgradeTarget()) {
      drupal_set_message('Unlocking upgrade target platform.');
      hosting_add_task($this->getUpgradeTarget(), 'unlock');
    }
  }

  /**
   * Return whether to automatically lock/unlock platforms.
   */
  public static function getAutomaticPlatformLockSetting() {
    return variable_get('automatic_distribution_platform_lock', TRUE);
  }

  /**
   * React to the update of a distribution node.
   */
  public function nodeUpdate() {
    if ($this->getAutomaticPlatformLockSetting()) {
      $this->unlockUpgradeTarget();
      $this->lockOutOfDatePlatforms();
    }
  }

}
