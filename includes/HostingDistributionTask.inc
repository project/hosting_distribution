<?php
/**
 * @file The HostingDistributionTask class.
 */

class HostingDistributionTask {

  /**
   * Implements hook_hosting_tasks().
   */
  static function hostingTasks() {
    return array(
      'site' => array(
        'upgrade' => array(
          'title' => t('Upgrade'),
          'description' => t('Upgrade the site to the latest platform for its distribution.'),
          'page arguments' => array('hosting_distribution_upgrade_site', 1),
          'access callback' => 'hosting_distribution_task_access',
          'access arguments' => array(1),
          'dialog' => TRUE,
        ),
      ),
      'platform' => array(
        'upgrade' => array(
          'title' => t('Upgrade'),
          'description' => t('Upgrade all sites on this platform to the latest platform for its distribution.'),
          'page arguments' => array('hosting_distribution_upgrade_platform', 1),
          'access callback' => 'hosting_distribution_task_access',
          'access arguments' => array(1),
          'dialog' => TRUE,
        ),
      ),
    );
  }

  /**
   * Define user permissions.
   */
  static function getPermissions() {
    return array(
      'create site upgrade task' => array(
        'title' => t('Create site upgrade task'),
      ),
      'create platform upgrade task' => array(
        'title' => t('Create platform upgrade task'),
      ),
    );
  }

  /**
   * Return paths that ought to be displayed in an overlay.
   */
  static function getOverlayPaths() {
    return array(
      'hosting/upgrade/compare/*' => array(
        'width' => 600,
      ),
    );
  }

  /**
   * Register a theme function.
   */
  static function registerTheme() {
    return array(
      'hosting_distribution_upgrade_comparison' => array(
        'variables' => array('packages' => NULL),
      ),
    );
  }

  /**
   * Compare package schema versions between the current and target platform in temp tables
   */
  function getUpgradeComparison($current, $target, $all = FALSE) {

    $current_tbl = _hosting_package_temporary_table($current);
    $target_tbl = _hosting_package_temporary_table($target);

    $packages = array();

    $result = db_query("SELECT c.nid, c.short_name,
      c.version as current_version, t.version as target_version,
      c.version_code as current_version_code, t.version_code as target_version_code,
      c.schema_version as current_schema, t.schema_version as target_schema, c.status AS enabled
      FROM $current_tbl c LEFT JOIN $target_tbl t ON c.nid = t.nid ORDER BY c.status DESC, short_name");

    while ($obj = $result->fetch()) {
      if (isset($obj->current_schema) && ((int) $obj->target_schema > 0) && ((int) $obj->current_schema > (int) $obj->target_schema)) {
        $obj->status = 'downgrade';
      }
      elseif ( ($obj->current_version_code > $obj->target_version_code)
         || is_null($obj->target_version_code)) {
        $obj->status = 'missing';
      }
      elseif (($obj->current_version_code < $obj->target_version_code) || ($obj->current_schema < $obj->target_schema)) {
        $obj->status = 'upgrade';
      }
      elseif ($all) {
        $obj->status = 'same';
      }
      else {
        continue;
      }
      $packages[$obj->nid] = $obj;
    }

    return theme("hosting_distribution_upgrade_comparison", array('packages' => $packages, 'all' => $all));
  }

  /**
   * Render a list of compared packages for upgrades.
   */
  function renderUpgradeComparison($variables) {
    $packages = $variables['packages'];
    $rows = array();

    $headers = array(t("Package"), t("Current"), t("Target"));
    foreach ($packages as $key => $instance) {
      $row = array();
      $row[] = array(
        'data' => $instance->short_name,
        'class' => array('hosting-status'),
      );
      $target_schema = ((int) $instance->target_schema == 0) ? $instance->current_schema : $instance->target_schema;
      $row[] = _hosting_migrate_version_display($instance->current_version, $instance->current_schema);
      $row[] = _hosting_migrate_version_display($instance->target_version, $target_schema, $instance->status);
      $rows[] = array(
        'data' => $row,
        'class' => ($instance->enabled) ? array('hosting-success') : array('hosting-info'),
      );
    }
    $options['attributes']['class'] = 'hosting-migrate-comparison-return';
    $back = l(t('Go back'), 'hosting_confirm/' . arg(1) . '/' . arg(2), $options);
    $link_path = implode('/', array_slice(explode('/', request_path()), 0, 6));
    if ($variables['all']) {
      $show = l(t('Show changed'), $link_path, $options);
    }
    else {
      $show = l(t('Show all'), $link_path . '/all', $options);
    }
    $show = '<div style="float: right">' . $show . '</div>';

    return "<div id='hosting-package-comparison'>" . $back . $show . theme('table', array('header' => $headers, 'rows' => $rows)) . $back . $show . "</div>";
  }

}
