<?php
/**
 * @file The HostingDistributionSiteUpgradeForm class.
 */

class HostingDistributionSiteUpgradeForm extends HostingForm {

  // The HostingDistributionSite associated with the form.
  private $site = FALSE;

  public function __construct(&$form, &$form_state, $node = FALSE) {
    parent::__construct($form, $form_state, $node);
    if ($this->node) {
      $this->site = new HostingDistributionSite($this->node);
      $this->log = $this->site->getDistribution()->log;
    }
  }

  /**
   * Page callback for the site upgrade confirmation form.
   */
  function build() {
    $profile_instance = $this->site->getTargetProfilePackageInstance();
    $status = hosting_package_comparison($this->node->nid, $profile_instance->iid);
    $description = t("Upgrades: !upgrades, Warnings: !missing, Errors: !errors | <a href='!url' class='hosting-package-comparison-link'>Compare packages</a>",
      array(
        '!upgrades' => $status['upgrade'],
        '!missing' => $status['missing'] + $status['downgrade'],
        '!errors' => $status['error'],
        '!url' => url('hosting_confirm/' . arg(1) . '/' . arg(2) . '/compare/' . $this->node->nid . '/' . $profile_instance->iid),
      )
    );

    if ($profile_instance && !$status['error']) {
      $this->form['prompt'] = array(
        '#type' => 'item',
        '#description' => 'Upgrade this site to the latest distribution platform?',
      );
      $this->form['description'] = array(
        '#type' => 'item',
        '#description' => $description,
      );

      $this->form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Confirm'),
      );
    }
    else {
      $this->form['error'] = array(
        '#type' => 'item',
        '#description' => 'While an upgrade target has been specified, it does not appear to be valid for this site. Please contact your platform administrator to resolve this error.',
      );
    }
    $this->form['cancel'] = array(
      '#type' => 'submit',
      '#value' => t('Cancel'),
    );

    return $this->form;
  }

  /**
   * Submit handler for site upgrades.
   */
  function submit() {
    if ($this->form_state['input']['op'] == 'Confirm') {
      $target = $this->site->getUpgradeTarget();
      hosting_add_task($this->node->nid, 'migrate', array(
        'target_platform' => $target,
        'new_uri' => $this->node->title,
        'new_db_server' => $this->node->db_server,
      ));

      $this->log->setTaskType('upgrade');
      $this->log->setNID($this->node->nid);
      $target_node = node_load($target);
      $this->log->success(t('Scheduled site migration to :target.', array(':target' => $target_node->title)));
    }
    $this->closeOverlay();
  }

}
