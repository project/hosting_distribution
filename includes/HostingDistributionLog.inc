<?php
/**
 * @file Logging-related functions.
 */

class HostingDistributionLog {

  // Verbosity constants
  const DEBUG = 4;
  const VERBOSE = 3;
  const NORMAL = 2;
  const QUIET = 1;
  const OFF = 0;

  // Unlogged messages.
  private $cache = array();

  // A hosting task to which we'll log.
  private $task = FALSE;

  // A hosting task type to which we'll log.
  private $task_type = 'unknown';

  // The log's current verbosity.
  private $verbosity = FALSE;

  // The NID of the node we're running our log against.
  private $nid = FALSE;

  public function __construct(HostingDistribution $distribution) {
    $this->distribution = $distribution;
    $this->nid = $this->distribution->getNid();
  }

  /**
   * Start logging to a new task.
   */
  public function newTask($task_type) {
    $this->cache = array();
    $this->task = FALSE;
    $this->setTaskType($task_type);
  }

  private function getVerbosity() {
    if (!$this->verbosity) {
      $this->verbosity = $this->distribution->getFieldValue('field_log_verbosity');
    }
    return $this->verbosity;
  }

  private function setVerbosity($verbosity) {
    $this->verbosity = $verbosity;
  }

  /**
   * Return the lowest level of verbosity where a given severity should be logged.
   */
  private function score($severity) {
    switch ($severity) {
      case 'error':
        return self::QUIET;
      case 'warning':
      case 'success':
      case 'ok':
        return self::NORMAL;
      case 'info':
      case 'message':
      case 'notice':
        return self::VERBOSE;
      case 'debug':
        return self::DEBUG;
      default:
        return self::OFF;
    }
  }

  private function shouldLog($severity) {
    return $this->getVerbosity() >= $this->score($severity);
  }

  /**
   * Store unlogged messages, in case a later message triggers output.
   */
  private function cache($message, $severity) {
    $this->cache[] = array(
      'message' => $message,
      'severity' => $severity,
    );
  }

  /**
   * Log any cached messages.
   */
  private function flushCache() {
    $this->setVerbosity(self::VERBOSE);
    foreach ($this->cache as $key => $item) {
      if ($this->score($item['severity']) < self::DEBUG) {
        $this->logToTask($item['message'], $item['severity']);
        unset($this->cache[$key]);
      }
    }
  }

  /**
   * Set the task type.
   */
  public function setTaskType($type) {
    $this->task_type = $type;
  }

  /**
   * Return the task type.
   */
  private function getTaskType() {
    return $this->task_type;
  }

  private function getNID() {
    return $this->nid;
  }

  public function setNID($nid) {
    $this->nid = $nid;
  }

  /**
   * Return an instantiated task, and ensure it's status is updated.
   */
  public function getTask() {
    if (!$this->task) {
      $this->task = hosting_add_task(
        $this->getNID(),
        $this->getTaskType(),
        array(),
        HOSTING_TASK_PROCESSING
      );
      register_shutdown_function('hosting_task_update_status', $this->task);
      $this->flushCache();
    }
    return $this->task;
  }

  /**
   * Log a message to a hosting queue task.
   */
  private function logToTask($message, $severity) {
    hosting_task_log($this->getTask()->vid, $severity, t($message));
  }

  /**
   * When appropriate, log a message to a hosting queue task.
   */
  public function log($message, $severity = 'info') {
    if ($this->shouldLog($severity)) {
      $this->logToTask($message, $severity);
    }
    else {
      $this->cache($message, $severity);
    }

    if (function_exists('drush_log') && drush_get_option('debug', FALSE)) {
      drush_log($message, $severity);
    }
  }

  /**
   * Log an error message.
   */
  public function error($message) {
    $this->log($message, 'error');
  }

  /**
   * Log a warning message.
   */
  public function warning($message) {
    $this->log($message, 'warning');
  }

  /**
   * Log a success message.
   */
  public function success($message) {
    $this->log($message, 'success');
  }

  /**
   * Log an informative message.
   */
  public function info($message) {
    $this->log($message, 'info');
  }

  /**
   * Log an debug message.
   */
  public function debug($message) {
    $this->log($message, 'debug');
  }

}
