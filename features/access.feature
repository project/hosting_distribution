Feature: Distributions view and menu item
  In order to manage platforms using distributions,
  As an Aegir platform manager,
  I need to be able to list distribution nodes.

  @api
  Scenario Outline: Access the main menu item
    Given I am logged in as a user with the "<role>" role
    When I am on the homepage
    Then I <access> see the link "Distributions" in the "navigation" region

    Examples:
      | role                    | access      |
      |  authenticated user     |  should not |
      |  aegir client           |  should not |
      |  aegir account manager  |  should not |
      |  administrator          |  should     |
      |  aegir administrator    |  should     |
      |  aegir platform manager |  should     |

