<?php
/**
 * @file
 * hosting_distribution.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function hosting_distribution_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'distribution_platform_list';
  $context->description = 'A list of platforms that are instances of this distribution';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'distribution' => 'distribution',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-b14d40515f4671fadd20e22b849c2d36' => array(
          'module' => 'views',
          'delta' => 'b14d40515f4671fadd20e22b849c2d36',
          'region' => 'content_bottom',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('A list of platforms that are instances of this distribution');
  $export['distribution_platform_list'] = $context;

  return $export;
}
