Aegir Distributions
===================

This module is documented in the main Aegir docs: http://docs.aegirproject.org/usage/advanced/distributions. TEMP in progress on https://github.com/aegir-project/documentation/pull/37


Testing
-------

To run tests, install an Aegir sandbox (such as with Valkyrie) then run:

    cd /path/to/hosting_distribution/
    composer install

This will install [Behat](http://behat.org), the [Behat Drupal extension](https://behat-drupal-extension.readthedocs.io), along with their dependencies.

You may want to alter the default Behat config, such as to alter the admin user's login credentials. To do so, first remove the symlink to the default `behat.yml` from the root of the project. Then copy that file back, so that you can alter the settings.

Running tests is the simply a matter of:

   ./bin/behat

