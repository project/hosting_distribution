<?php
/**
 * @file
 * hosting_distribution.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function hosting_distribution_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-distribution-field_log_verbosity'.
  $field_instances['node-distribution-field_log_verbosity'] = array(
    'bundle' => 'distribution',
    'default_value' => array(
      0 => array(
        'value' => 2,
      ),
    ),
    'deleted' => 0,
    'description' => 'Set how frequent and detailed logging should be.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_log_verbosity',
    'label' => 'Log level',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 10,
    ),
  );

  // Exported field_instance: 'node-distribution-field_upgrade_target'.
  $field_instances['node-distribution-field_upgrade_target'] = array(
    'bundle' => 'distribution',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The platform towards which an `upgrade` task will migrate sites and platforms.

At least one platform must reference this distribution for this field to present any valid options.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'entityreference',
        'settings' => array(
          'link' => 1,
        ),
        'type' => 'entityreference_label',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_upgrade_target',
    'label' => 'Upgrade target',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-platform-field_distribution'.
  $field_instances['node-platform-field_distribution'] = array(
    'bundle' => 'platform',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'entityreference',
        'settings' => array(
          'link' => 1,
        ),
        'type' => 'entityreference_label',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_distribution',
    'label' => 'Distribution',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => -4,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Distribution');
  t('Log level');
  t('Set how frequent and detailed logging should be.');
  t('The platform towards which an `upgrade` task will migrate sites and platforms.

At least one platform must reference this distribution for this field to present any valid options.');
  t('Upgrade target');

  return $field_instances;
}
