<?php
/**
 * @file
 * Drupal Form API hook implementations and callbacks.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function hosting_distribution_updates_form_distribution_node_form_alter(&$form, &$form_state, $form_id) {
  $makefile_form = new HostingDistributionUpdateForm($form, $form_state);
  $makefile_form->alterDistributionNodeForm();
}

/**
 * Validate callback for the distribution form.
 */
function hosting_distribution_updates_validate(&$form, &$form_state) {
  $update_form = new HostingDistributionUpdateForm($form, $form_state);
  $update_form->validate();
}

