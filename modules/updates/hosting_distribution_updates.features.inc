<?php
/**
 * @file
 * hosting_distribution_updates.features.inc
 */

/**
 * Implements hook_views_api().
 */
function hosting_distribution_updates_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
