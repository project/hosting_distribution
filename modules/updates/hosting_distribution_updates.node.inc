<?php
/**
 * @file
 * Drupal Node API hook implementations.
 */

/**
 * Implements hook_node_view().
 */
function hosting_distribution_updates_node_view($node, $view_mode, $langcode) {
  if ($node->type == 'distribution') {
    $distribution = new HostingDistribution($node);
    $update = new HostingDistributionUpdate($distribution);
    $update->nodeView();
  }
}

/**
 * Implements hook_node_insert().
 */
function hosting_distribution_updates_node_insert($node) {
  $node->is_new = FALSE;
  return hosting_distribution_updates_node_update($node);
}

/**
 * Implements hook_node_update().
 */
function hosting_distribution_updates_node_update($node) {
  if ($node->type == 'distribution') {
    $update = new HostingDistributionUpdate(new HostingDistribution($node));
    $update->nodeUpdate();
  }
}

