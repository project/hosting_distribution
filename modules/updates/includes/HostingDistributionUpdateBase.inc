<?php
/**
 * @file The HostingDistributionUpdateBase abstract class.
 */

abstract class HostingDistributionUpdateBase {

  // A label for this type of update.
  protected $label = '';

  // A machine name for this type of update.
  protected $machine_name = '';

  // A description of this type of update.
  protected $description = '';

  // Fields that should be formatted in the Aegir style on distribution nodes.
  protected $info_fields = array();

  // Fields that should be made read-only on distribution node forms.
  protected $readonly_fields = array();

  // Fields that should be hidden on distribution node forms.
  protected $hidden_fields = array();

  // Other fields managed by this extention.
  protected $other_fields = array();

  // The HostingDistribution object that is being updated.
  protected $distribution = FALSE;

  // A HostingDistributionLog object.
  protected $log = FALSE;

  // A HostingDistributionUpdateForm object.
  protected $form = FALSE;

  public function __construct($distribution = FALSE) {
    if ($distribution) {
      $this->setDistribution($distribution);
      $this->log = $this->distribution->log;
    }
  }

  public function setDistribution(HostingDistribution $distribution) {
    $this->distribution = $distribution;
  }

  abstract public function update();

  /**
   * A validate callback to ensure the Drush makefile is accessible.
   */
  public function validate(&$form, &$form_state) {
    $this->form = new HostingDistributionUpdateForm($form, $form_state);
  }

  /**
   * Return a hash of the current makefile.
   */
  abstract public function calculateSignature();

  /**
   * Alter a new platform before it is saved.
   */
  abstract public function alterPlatform(HostingDistributionPlatform &$platform);

  /**
   * Return a hosting queue definition.
   */
  public function getQueue() {
    return array(
      'distribution_' . $this->getMachineName() . '_update' => array(
        'type' => 'batch',
        'name' => t(':label update queue', array(':label' => $this->getLabel())),
        'description' => t('Check for :description.', array(':description' => strtolower($this->getDescription()))),
        'total_items' => count($this->getDistributions()),
        'frequency' => strtotime("1 hour", 0),
        'singular' => t('distribution'),
        'plural' => t('distributions'),
      )
    );
  }

  /**
   * Return a list of NIDs for distributions that use a Drush Makefile.
   */
  public function getDistributions() {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node')
      ->entityCondition('bundle', 'distribution')
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldCondition('field_update_type', 'value', $this->getMachineName());

    $result = $query->execute();
    if (isset($result['node'])) {
      return array_keys($result['node']);
    }
    else {
      return array();
    }
  }

  public function getLabel() {
    return $this->label;
  }

  public function getAllFields() {
    return array_merge($this->info_fields, $this->readonly_fields, $this->other_fields);
  }

  public function getReadonlyFields() {
    return $this->readonly_fields;
  }

  public function getHiddenFields() {
    return $this->hidden_fields;
  }

  public function getInfoFields() {
    return $this->info_fields;
  }

  public function getOtherFields() {
    return $this->other_fields;
  }

  public function getDescription() {
    return $this->description;
  }

  public function getMachineName() {
    return $this->machine_name;
  }

}
