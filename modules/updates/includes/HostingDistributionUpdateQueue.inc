<?php
/**
 * @file The HostingDistributionUpdateQueue class.
 */

class HostingDistributionUpdateQueue {

  // The types of update available for distributions.
  public $types = array();

  // The type of update.
  public $type = array();

  public function __construct($type = FALSE) {
    $this->types = module_invoke_all('distribution_update_types');
    $this->type = $type;
  }

  /**
   * Return the hosting queue definitions for all update types.
   */
  public function getQueues() {
    $queues = array();
    foreach ($this->types as $type => $class) {
      $update = new $class();
      $queues = array_merge($queues, $update->getQueue());
    }
    return $queues;
  }

  /**
   * Process a given update queue.
   */
  public function processQueue() {
    $class = $this->types[$this->type];
    $update_type = new $class();
    $distributions = node_load_multiple($update_type->getDistributions());
    foreach ($distributions as $nid => $node) {
      $update = new HostingDistributionUpdate(new HostingDistribution($node));
      $update->log->newTask('check');
      $update->log->info(t('Processing distribution :type update queue.', array(':type' => $update->type->getLabel())));
      $update->log->info(t('Checking if update is required.'));
      if ($update->updateRequired()) {
        $update->log->success(t('Check complete. Update is required.'));
        $update->log->info(t('Scheduling distribution update.'));
        $update->update();
      }
      else {
        $update->log->info(t('Distribution is current. No update is required.'));
      }
    }
  }

}
