<?php
/**
 * @file The HostingDistributionUpdate class.
 */

class HostingDistributionUpdate {

  // The HostingDistribution object that is being updated.
  public $distribution = FALSE;

  // The types of update available for distributions.
  public $types = array();

  // An update type object as set for this distribution.
  public $type = FALSE;

  // Fields to format in line with Aegir standards.
  public $info_fields = array('field_update_type', 'field_update_signature', 'field_update_web_server');

  // Fields that should be made read-only on distribution node forms.
  public $readonly_fields = array('field_update_signature');

  // Other fields managed by this extention.
  public $update_fields = array('field_update_build', 'field_update_signature', 'field_update_web_server');

  // A HostingDistributionLog object.
  public $log = FALSE;

  public function __construct(HostingDistribution $distribution) {
    $this->distribution = $distribution;

    $this->types = module_invoke_all('distribution_update_types');
    $type = $this->distribution->getFieldValue('field_update_type');
    if (array_key_exists($type, $this->types)) {
      $class = $this->types[$type];
      if (class_exists($class)) {
        $this->type = new $class($this->distribution);
      }
    }

    $this->log = $this->distribution->log;
  }

  private function getSignature() {
    return $this->distribution->getFieldValue('field_update_signature');
  }

  public function updateRequired() {
    if (function_exists('drush_get_option') && drush_get_option('force', FALSE)) {
      $this->log->info(t('Forcing distribution update.'));
      return TRUE;
    }
    return $this->getSignature() !== $this->type->calculateSignature();
  }

  /**
   * Trigger a distribution update.
   */
  public function update() {
    $this->triggerBuild();
    // Updates are triggered from hook_node_update();
    $this->distribution->save();
  }

  /**
   * Return whether to trigger a new platform build.
   */
  private function getBuildTrigger() {
    return $this->distribution->getFieldValue('field_update_build');
  }

  /**
   * Set whether to trigger a new platform build.
   */
  private function setBuildTrigger($value) {
    $this->distribution->setFieldValue('field_update_build', $value);
  }

  /**
   * Block a new platform build.
   */
  private function resetBuildTrigger() {
    $this->setBuildTrigger(0);
  }

  /**
   * Trigger a new platform build.
   */
  private function triggerBuild() {
    $this->setBuildTrigger(1);
  }

  /**
   * Update the source signature.
   */
  private function updateSignature() {
    return $this->distribution->setFieldValue('field_update_signature', $this->type->calculateSignature());
  }

  /**
   * Logging wrapper around source signature update.
   */
  private function logUpdateSignature() {
    $this->log->info(t('Setting new update signature.'));
    $this->log->debug(t('New update signature: :signature.', array(':signature' => $this->type->calculateSignature())));
    if ($this->updateSignature()) {
      $this->log->success(t('Set new update signature.'));
    }
    else {
      $this->log->warning(t('Failed to set new update signature.'));
    }
  }

  /**
   * Hand off update to update type plugin.
   */
  private function typeUpdate() {
    return $this->type->update();
  }

  /**
   * Logging wrapper around update type plugin update.
   */
  private function logTypeUpdate() {
    $this->log->info(t('Handing off distribution update to update type plugin.'));
    $this->log->debug(t('Calling :class::update().', array(':class' => get_class($this->type))));
    if ($this->typeUpdate()) {
      $this->log->success(t('Update type plugin update successful.'));
    }
    else {
      $this->log->warning(t('Update type plugin update failed.'));
    }
  }

  private function generatePlatformTitle() {
    // Generate a human-readable timestamp.
    $date = date(DATE_W3C);
    // Strip the timezone suffix.
    $date = substr($date, 0, strpos($date, "+"));
    return $this->distribution->getTitle() . ' ' . $date;
  }

  private function generateMachineName($input) {
    #return preg_replace('([^a-z0-9_\.\-])', '_', strtolower($input));
    return preg_replace("/[!\W]/", "", $input);
  }

  private function generatePublishPath($title) {
    $base_path = variable_get('hosting_platform_base_path', '/var/aegir/platforms/');
    return $base_path . $this->generateMachineName($title);
  }

  private function buildPlatformNode() {
    $node = new stdClass();
    $node->type = 'platform';
    $node->title = $this->generatePlatformTitle();
    $this->log->debug('$node->title: ' . $node->title);
    $node->publish_path = $this->generatePublishPath($node->title);
    $this->log->debug('$node->publish_path: ' . $node->publish_path);
    $node->language = $this->distribution->getLanguage();
    $this->log->debug('$node->language: ' . $node->language);
    $node->web_server = $this->distribution->getEntityReference('field_update_web_server');
    $this->log->debug('$node->web_server: ' . $node->web_server);
    // We need to fully save the node, in order for the distribution field to
    // be properly set, *before* running a 'verify' task to build the platform.
    $node->no_verify = TRUE;
    return $node;
  }

  private function buildPlatform() {
    $platform = new HostingDistributionPlatform($this->buildPlatformNode());
    $platform->setEntityReference('field_distribution', $this->distribution->getNid());
    return $platform;
  }

  private function logBuildPlatform() {
    $this->log->info(t('Creating new platform for :distribution based on :type.', array(
      ':distribution' => $this->distribution->getTitle(),
      ':type' => $this->type->getLabel(),
    )));

    $platform = $this->buildPlatform();

    $this->log->info('Passing base platform to update plugin for modification.');
    $this->log->debug(t('Calling :class::alterPlatform().', array(':class' => get_class($this->type))));
    if ($this->type->alterPlatform($platform)) {
      $this->log->success(t('Update plugin successfully modified platform.'));
    }
    else {
      $this->log->error(t('Update plugin failed to modify platform.'));
    }

    $this->log->info('Saving new platform node.');
    $this->log->debug(t('Calling :class::save().', array(':class' => get_class($platform))));
    if ($platform_id = $platform->save()) {
      $this->log->success(t('New platform saved.'));
    }
    else {
      $this->log->error(t('Failed to save new platform.'));
    }

    $this->log->info(t('Queuing verify task for new platform node.'));
    $this->log->debug('$platform_id: ' . $platform_id);
    if (hosting_add_task($platform_id, 'verify')) {
      $this->log->success(t('Queued verify task for new platform node.'));
    }
    else {
      $this->log->error(t('Failed to queue verify task for new platform node.'));
    }

    return $platform_id;
  }

  private function setUpgradeTarget($platform_id) {
    $this->distribution->setEntityReference('field_upgrade_target', $platform_id);
  }

  /**
   * Callback from hook_node_update().
   */
  public function nodeUpdate() {
    $this->log->newTask('update');
    if ($this->type) {
      if ($this->getBuildTrigger()) {
        $this->logUpdateSignature();
        $this->logTypeUpdate();
        $platform_id = $this->logBuildPlatform();
        $this->log->info('Setting upgrade target.');
        $this->setUpgradeTarget($platform_id);
        $this->log->info('Resetting build trigger.');
        $this->resetBuildTrigger();
        $this->log->info('Saving Distribution node.');
        $this->distribution->save();
      }
    }
  }

  /**
   * Callback from hook_node_view().
   */
  public function nodeView() {
    foreach ($this->getInfoFields() as $field) {
      $this->distribution->fixNodeFieldDisplay($field);
    }
  }

  /**
   * Combine the generic update info fields with the type plugin's.
   */
  public function getInfoFields() {
    if ($this->type) {
      $class = $this->type;
      $update_type = new $class();
      return array_merge($this->info_fields, $update_type->getInfoFields());
    }
  }

}
