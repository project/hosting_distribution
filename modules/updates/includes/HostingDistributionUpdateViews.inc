<?php
/**
 * @file The HostingDistributionUpdateViews class.
 */

class HostingDistributionUpdateViews {

  /**
   * Alter Views data.
   */
  public function dataAlter(&$data) {
    // Allow for filtering the web server field on distributions.
    $data['hosting_service']['service']['filter'] = array(
      'handler' => 'views_handler_filter_string',
    );
  }


}
