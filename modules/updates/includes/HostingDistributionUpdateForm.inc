<?php
/**
 * @file The HostingDistributionMakefileForm class.
 */

class HostingDistributionUpdateForm extends HostingForm {

  public function __construct(&$form, &$form_state, $node = FALSE) {
    parent::__construct($form, $form_state, $node);
    $this->distribution = new HostingDistribution($this->node);
    $this->update = new HostingDistributionUpdate($this->distribution);
  }

  private function getUpdateTypes() {
    $types = $this->update->types;
    foreach ($types as $type => $class) {
      $update_type = new $class();
      $types[$type] = $update_type->getLabel();
    }
    return $types;
  }

  private function getUpdateTypeOptions() {
    $options = array('_none' => '- None -');
    return array_merge($options, $this->getUpdateTypes());
  }

  /**
   * Form validate handler.
   */
  public function validate() {
    $types = $this->update->types;
    $type = $this->distribution->getFieldValue('field_update_type');
    $class = $types[$type];
    if (class_exists($class)) {
      $update = new $class();
      return $update->validate($this->form, $this->form_state);
    }
  }

  /**
   * Called from hook_form_FORM_ID_alter() for distribution nodes.
   */
  public function alterDistributionNodeForm() {
    $this->addValidateHandler('hosting_distribution_updates_validate');
    $this->lockReadOnlyFields();
    $this->hideHiddenFields();
    $this->setUpdateTypeOptions();
    $this->setUpdateTypeFieldsets();
    $this->setDefaultWebServer();
  }

  protected function setDefaultWebServer() {
    $web_server =& $this->form['update_type_fieldset']['field_update_web_server'];
    unset($web_server[$web_server['#language']]['#options']['_none']);
  }

  private function lockReadOnlyFields() {
    foreach ($this->getReadOnlyFields() as $field) {
      $this->makeFieldReadOnly($field);
    }
  }

  private function hideHiddenFields() {
    foreach ($this->getHiddenFields() as $field) {
      $this->disableField($field);
    }
  }

  private function getReadOnlyFields() {
    foreach ($this->update->types as $type => $class) {
      $update_type = new $class();
      $fields = array_merge($fields, $update_type->getReadonlyFields());
    }
    return $fields;
  }

  private function getHiddenFields() {
    $fields = array('field_update_signature');
    foreach ($this->update->types as $type => $class) {
      $update_type = new $class();
      $fields = array_merge($fields, $update_type->getHiddenFields());
    }
    return $fields;
  }

  private function setUpdateTypeOptions() {
    $this->form['field_update_type']['und']['#options'] = $this->getUpdateTypeOptions();
    $type = $this->distribution->getFieldValue('field_update_type') ?: '_none';
    $this->form['field_update_type']['und']['#default_value'] = $type;
  }

  private function setUpdateTypeFieldsets() {
    $visible = array();
    foreach ($this->getUpdateTypes() as $type => $class) {
      $visible[] = array('value' => $type);
    }
    $this->form['update_type_fieldset'] = array(
      '#type' => 'fieldset',
      '#title' => 'Update type',
      '#weight' => 10,
      '#states' => array(
        'visible' => array(
          ':input[name="field_update_type[und]"]' => $visible,
        ),
      ),
    );

    foreach ($this->update->types as $type => $class) {
      $update_type = new $class();
      $this->form['update_type_fieldset']["{$type}_fieldset"] = array(
        '#type' => 'fieldset',
        '#title' => $update_type->getLabel(),
        '#weight' => -10,
        '#states' => array(
          'visible' => array(
            ':input[name="field_update_type[und]"]' => array('value' => $type),
          ),
        ),
      );
      foreach ($update_type->getAllFields() as $field) {
        $this->form['update_type_fieldset']["{$type}_fieldset"][$field] = $this->form[$field];
        unset($this->form[$field]);
      }
    }

    foreach ($this->update->update_fields as $field) {
      $this->form['update_type_fieldset'][$field] = $this->form[$field];
      unset($this->form[$field]);
    }

    $this->form['field_log_verbosity']['#weight'] = 20;

  }

}
