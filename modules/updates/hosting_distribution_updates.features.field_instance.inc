<?php
/**
 * @file
 * hosting_distribution_updates.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function hosting_distribution_updates_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-distribution-field_update_build'.
  $field_instances['node-distribution-field_update_build'] = array(
    'bundle' => 'distribution',
    'default_value' => array(
      0 => array(
        'value' => 1,
      ),
    ),
    'deleted' => 0,
    'description' => 'Trigger a new platform build, overriding automatic checks.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 7,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_update_build',
    'label' => 'Trigger build',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-distribution-field_update_signature'.
  $field_instances['node-distribution-field_update_signature'] = array(
    'bundle' => 'distribution',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 5,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_update_signature',
    'label' => 'Update signature',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-distribution-field_update_type'.
  $field_instances['node-distribution-field_update_type'] = array(
    'bundle' => 'distribution',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The method by which new distribution platforms will be automatically created.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 6,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_update_type',
    'label' => 'Update type',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-distribution-field_update_web_server'.
  $field_instances['node-distribution-field_update_web_server'] = array(
    'bundle' => 'distribution',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Select the web server onto which to deploy this distribution.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'link' => 1,
        ),
        'type' => 'entityreference_label',
        'weight' => 8,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_update_web_server',
    'label' => 'Web server',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 8,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Select the web server onto which to deploy this distribution.');
  t('The method by which new distribution platforms will be automatically created.');
  t('Trigger a new platform build, overriding automatic checks.');
  t('Trigger build');
  t('Update signature');
  t('Update type');
  t('Web server');

  return $field_instances;
}
