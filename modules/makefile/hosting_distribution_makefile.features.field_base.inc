<?php
/**
 * @file
 * hosting_distribution_makefile.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function hosting_distribution_makefile_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_drush_makefile'.
  $field_bases['field_drush_makefile'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_drush_makefile',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_drush_makefile_contents'.
  $field_bases['field_drush_makefile_contents'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_drush_makefile_contents',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  return $field_bases;
}
