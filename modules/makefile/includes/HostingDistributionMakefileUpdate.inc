<?php
/**
 * @file
 */

class HostingDistributionMakefileUpdate extends HostingDistributionUpdateBase {

  // A label for this type of update.
  protected $label = 'Makefile';

  // A machine name for this type of update.
  protected $machine_name = 'makefile';

  // A description of this type of update.
  protected $description = 'Makefile-based distribution updates';

  // Fields that should be formatted in the Aegir style on distribution nodes.
  protected $info_fields = array('field_drush_makefile');

  // Fields that should be made read-only on distribution node forms.
  protected $readonly_fields = array('field_drush_makefile_contents');

  public function update() {
    $this->log->info('Performing makefile plugin updates.');
    if ($this->logSetMakefileContents()) {
      $this->log->success('Performed makefile plugin updates.');
      return TRUE;
    }
    else {
      $this->log->warning('Failed to perform makefile plugin updates.');
      return FALSE;
    }
  }

  /**
   * A validate callback to ensure the Drush makefile is accessible.
   */
  public function validate(&$form, &$form_state) {
    parent::validate($form, $form_state);
    if ($makefile = $this->form->getFormStateValue('field_drush_makefile')) {
      if (!file_get_contents($makefile)) {
        return form_error($this->form->form['update_type_fieldset']['makefile_fieldset']['field_drush_makefile'], t('Cannot resolve Drush makefile. Please ensure that you have the correct path or URL.'));
      }
    }
  }

  /**
   * Return a hash of the current makefile.
   */
  public function calculateSignature() {
    return hash('md5', $this->getMakefileContents());
  }

  /**
   * Return a hash of the current Drush makefile.
   */
  private function getMakefile() {
    return $this->distribution->getFieldValue('field_drush_makefile');
  }

  /**
   * Fetch the contents of the specified Drush makefile.
   */
  private function getMakefileContents() {
    if ($contents = file_get_contents($this->getMakefile())) {
      return $contents;
    }
  }

  /**
   * Save the contents of the specified Drush makefile to the distribution node.
   */
  private function setMakefileContents() {
    return $this->distribution->setFieldValue('field_drush_makefile_contents', $this->getMakefileContents());
  }

  /**
   * Logging wrapper around setting makefile contents.
   */
  private function logSetMakefileContents() {
    $this->log->info(t('Setting new Drush makefile contents.'));
    $this->log->debug(t('New Drush makefile path: :path.', array(':path' => $this->getMakefile())));
    $this->log->debug(t("New Drush makefile contents: :contents.", array(':contents' => $this->getMakefileContents())));
    if ($this->setMakefileContents()) {
      $this->log->success(t('Set Drush makefile contents.'));
      return TRUE;
    }
    else {
      $this->log->warning(t('Failed to set Drush makefile contents.'));
      return FALSE;
    }
  }

  /**
   * Alter a new platform before it is saved.
   */
  public function alterPlatform(HostingDistributionPlatform &$platform) {
    return $platform->getNode()->frommakefile = array('makefile' => $this->getMakefile());
  }

}
