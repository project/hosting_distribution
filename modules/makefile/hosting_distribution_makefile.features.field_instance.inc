<?php
/**
 * @file
 * hosting_distribution_makefile.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function hosting_distribution_makefile_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-distribution-field_drush_makefile'.
  $field_instances['node-distribution-field_drush_makefile'] = array(
    'bundle' => 'distribution',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The absolute path on the filesystem or public URL of a Drush makefile that will be used to create platforms in this distribution.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_drush_makefile',
    'label' => 'Drush makefile',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => -3,
    ),
  );

  // Exported field_instance: 'node-distribution-field_drush_makefile_contents'.
  $field_instances['node-distribution-field_drush_makefile_contents'] = array(
    'bundle' => 'distribution',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_drush_makefile_contents',
    'label' => 'Drush makefile contents',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => -1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Drush makefile');
  t('Drush makefile contents');
  t('The absolute path on the filesystem or public URL of a Drush makefile that will be used to create platforms in this distribution.');

  return $field_instances;
}
