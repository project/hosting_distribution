<?php
/**
 * @file
 */

class HostingDistributionGitUpdate extends HostingDistributionUpdateBase {

  // A label for this type of update.
  protected $label = 'Git';

  // A machine name for this type of update.
  protected $machine_name = 'git';

  // A description of this type of update.
  protected $description = 'Git-based distribution updates';

  // Fields that should be formatted in the Aegir style on distribution nodes.
  protected $info_fields = array('field_git_repository', 'field_git_reference', 'field_git_docroot');

  protected $git_repository = FALSE;

  protected $git_reference = FALSE;

  protected $git_docroot = FALSE;

  public function __construct($distribution = FALSE) {
    parent::__construct($distribution);
    if ($distribution) {
      $this->setGitInfo();
    }
  }

  protected function setGitInfo() {
    $this->git_repository = $this->distribution->getFieldValue('field_git_repository');
    $this->git_reference = $this->distribution->getFieldValue('field_git_reference');
    $this->git_docroot = $this->distribution->getFieldValue('field_git_docroot');
  }

  public function update() {
    $this->log->debug('No Git plugin updates required.');
    return TRUE;
  }

  /**
   * A validate callback to ensure the Drush makefile is accessible.
   */
  public function validate(&$form, &$form_state) {
    parent::validate($form, $form_state);
    if (!($this->calculateSignature())) {
      return form_error($this->form->form['update_type_fieldset']['makefile_fieldset']['field_git_repository'],
        t('Cannot access the :ref reference from the :repo repository.', array(
          ':repo' => $this->getGitRepository(),
          ':ref' => $this->getGitReference(),
      )));
    }
  }

  /**
   * Return a hash of the current makefile.
   */
  public function calculateSignature() {
    // TODO: Specify an SSH identity that has access to private repos.
    // ssh-agent bash -c 'ssh-add /somewhere/yourkey; git clone git@github.com:user/project.git'
    // N.B. This obviously means that the web user will have read access to a
    // private repo.
    if ($ls_remote = shell_exec('git ls-remote ' . $this->getGitRepository() . ' ' . $this->getGitReference())) {
      return strtok($ls_remote, "\t");
    }
    else {
      return FALSE;
    }
  }

  protected function getGitRepository() {
    if (!$this->git_repository && $this->form) {
      $this->git_repository = $this->form->getFormStateValue('field_git_repository');
    }
    return $this->git_repository;
  }

  protected function getGitReference() {
    if (!$this->git_reference && $this->form) {
      $this->git_reference = $this->form->getFormStateValue('field_git_reference');
    }
    return $this->git_reference;
  }

  protected function getGitDocroot() {
    if (!$this->git_docroot && $this->form) {
      $this->git_docroot = $this->form->getFormStateValue('field_git_docroot');
    }
    return $this->git_docroot;
  }

  /**
   * Alter a new platform before it is saved.
   */
  public function alterPlatform(HostingDistributionPlatform &$platform) {
    $platform->node->git = array(
      'repo_path' => $platform->node->publish_path,
      'repo_url' => $this->getGitRepository(),
      'git_ref' => $this->getGitReference() ?: 'master',
      'repo_docroot' => $this->getGitDocroot(),
      'pull_method' => 0,
    );
    $platform->node->publish_path = $platform->node->publish_path . '/' . $this->getGitDocroot();
    return TRUE;
  }

}
