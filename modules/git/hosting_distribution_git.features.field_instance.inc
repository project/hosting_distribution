<?php
/**
 * @file
 * hosting_distribution_git.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function hosting_distribution_git_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-distribution-field_git_docroot'.
  $field_instances['node-distribution-field_git_docroot'] = array(
    'bundle' => 'distribution',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'If Drupal is not in the root of the repository, enter the relative path within the repository that points to the correct subfolder.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 11,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_git_docroot',
    'label' => 'Git docroot',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 13,
    ),
  );

  // Exported field_instance: 'node-distribution-field_git_reference'.
  $field_instances['node-distribution-field_git_reference'] = array(
    'bundle' => 'distribution',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'A Git branch or tag.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 10,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_git_reference',
    'label' => 'Git reference',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 12,
    ),
  );

  // Exported field_instance: 'node-distribution-field_git_repository'.
  $field_instances['node-distribution-field_git_repository'] = array(
    'bundle' => 'distribution',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'A Git repository from which we\'ll create platforms in this distribution.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_git_repository',
    'label' => 'Git repository URL',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => -3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('A Git branch or tag.');
  t('A Git repository from which we\'ll create platforms in this distribution.');
  t('Git docroot');
  t('Git reference');
  t('Git repository URL');
  t('If Drupal is not in the root of the repository, enter the relative path within the repository that points to the correct subfolder.');

  return $field_instances;
}
