<?php
/**
 * @file
 * hosting_distribution_stock.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function hosting_distribution_stock_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-distribution-field_stock_distribution'.
  $field_instances['node-distribution-field_stock_distribution'] = array(
    'bundle' => 'distribution',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Choose the stock distribution from drupal.org to deploy.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 9,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_stock_distribution',
    'label' => 'Stock distribution',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 11,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Choose the stock distribution from drupal.org to deploy.');
  t('Stock distribution');

  return $field_instances;
}
