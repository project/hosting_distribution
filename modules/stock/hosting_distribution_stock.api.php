<?php

/**
 * Alter the list of available stock distributions.
 */
function hook_hosting_stock_distribution_list_alter(&$distros, $api_version) {
  foreach ($distros as $name => $title) {
    // Implement a whitelist.
    if (!in_array($name, ['openatrium', 'thunder'])) {
      unset($distros[$name]);
    }
  }
}

