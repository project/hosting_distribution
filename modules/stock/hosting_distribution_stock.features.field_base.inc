<?php
/**
 * @file
 * hosting_distribution_stock.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function hosting_distribution_stock_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_stock_distribution'.
  $field_bases['field_stock_distribution'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_stock_distribution',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(),
      'allowed_values_function' => 'hosting_distribution_stock_get_stock_options_list',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
