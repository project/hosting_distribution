<?php
/**
 * @file
 * Drupal Form API hook implementations and callbacks.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function hosting_distribution_stock_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'hosting_distribution_settings_form') {
    $settings_form = new HostingDistributionStockUpdateForm($form, $form_state);
    $settings_form->alterSettingsForm();
  }
  if ($form_id == 'distribution_node_form') {
    $distribution_node_form = new HostingDistributionStockUpdateForm($form, $form_state);
    $distribution_node_form->alterDistributionNodeForm();
  }
}

/**
 * Validate callback for the settings form.
 */
function hosting_distribution_stock_settings_validate(&$form, &$form_state) {
  $settings_form = new HostingDistributionStockUpdateForm($form, $form_state);
  $settings_form->validateSettingsForm();
}

/**
 * Callback to build version-specific forms to select distributions to support.
 */
function hosting_distribution_stock_support_form($form, &$form_state, $api_version) {
  $support_form = new HostingDistributionStockUpdateForm($form, $form_state, $api_version);
  return $support_form->getSupportForm();
}

/**
 * Callback to submit version-specific support forms.
 */
function hosting_distribution_stock_support_form_submit($form, &$form_state) {
  $support_form = new HostingDistributionStockUpdateForm($form, $form_state);
  return $support_form->submitSupportForm();
}

/**
 * Callback to populate the list of valid options for stock distributions.
 */
function hosting_distribution_stock_get_stock_options_list() {
  $stock_options = new HostingDistributionStockUpdateForm($form = [], $form_state = []);
  $stock_options->getDistributionNodeFormStockOptions();
}
