<?php
/**
 * @file The HostingDistributionStockUpdateForm class.
 */

class HostingDistributionStockUpdateForm extends HostingForm {

  const CACHE_ID = 'hosting_distribution_stock_distro_list';

  protected $distro_list_url = 'https://www.drupal.org/project/project_distribution/index/full?project-status=!status&drupal_core=!api_version';

  protected $project_status = 'full';

  protected $api_version = FALSE;

  protected static $distro_version_taxonomy = [
    '7.x' => '103',
    '8.x' => '7234',
  ];

  protected $security_statuses = [
    'covered' => 'Covered',
    'beta' => 'Beta releases are not covered by Drupal security advisories.',
    'opt-out' => 'Project has not opted into security advisory coverage!',
  ];

  protected $maintenance_statuses = [
    'active' => 'Actively maintained',
    'seeking' => 'Seeking co-maintainer(s)',
    'minimal' => 'Minimally maintained',
    'unsupported' => 'Unsupported',
  ];

  protected $project_statuses = [
    'published' => 'published',
    'unsupported' => 'unsupported',
  ];

  public function __construct(&$form, &$form_state, $api_version = FALSE) {
    parent::__construct($form, $form_state);
    $this->api_version = $api_version;
  }

  protected function filterByStatusType($type) {
    return variable_get("stock_distribution_filter_by_{$type}_status", TRUE);
  }

  public function alterSettingsForm() {
    foreach (['security', 'maintenance', 'project'] as $type) {
      $this->form["stock_distribution_filter_by_{$type}_status"] = [
        '#type' => 'checkbox',
        '#title' => t('Filter available stock distributions by :type status.', [':type' => $type]),
        '#default_value' => $this->filterByStatusType($type),
      ];
      $this->form[$type] = [
        '#type' => 'fieldset',
        '#description' => t('Choose which :type statuses to support:', [':type' => $type]),
        '#states' => array(
          'invisible' => array(
            ":input[name='stock_distribution_filter_by_{$type}_status']" => array('checked' => FALSE),
          ),
        ),
      ];
      $type_statuses = $type . '_statuses';
      $this->form[$type]["stock_distribution_{$type}_statuses"] = [
        '#type' => 'checkboxes',
        '#options' => $this->$type_statuses,
        '#default_value' => $this->getStatuses($type),
      ];
    }
  }

  public function alterDistributionNodeForm() {
    if ($this->isANewNode()) {
      $this->disableField('field_upgrade_target');
    }
    $this->setFieldListOptions('field_stock_distribution', $this->getDistributionNodeFormStockOptions());
  }

  public function getDistributionNodeFormStockOptions() {
    $distros = [];
    foreach (self::getApiVersions() as $api_version) {
      $this->api_version = $api_version;
      $supported = array_filter($this->getSupportedStockDistributionsByApiVersion($api_version));
      foreach ($this->getDistroList() as $name => $title) {
        if ($supported[$name]) {
          $distros[$name . "[$api_version]"] = $title;
        }
      }
    }
    return $distros;
  }

  protected function getStatuses($type) {
    $statuses = &drupal_static(__FUNCTION__, array());
    if (empty($statuses) || !isset($statuses[$type])) {
      $type_statuses = $type . '_statuses';
      $options = $this->$type_statuses;
      // Default to strictest, i.e., only the first option.
      $counter = 0;
      foreach ($options as $key => $value) {
        if ($counter) {
          $options[$key] = FALSE;
        }
        else {
          $options[$key] = $key;
        }
        $counter++;
      }
      $statuses[$type] = variable_get("stock_distribution_{$type}_statuses", $options);
    }
    return $statuses[$type];
  }

  public static function getMenuItems() {
    $items = [];

    $items['admin/hosting/distributions/settings'] = array(
      'title' => t('Settings'),
      'description' => 'Configure settings related to distributions.',
      'page arguments' => array('hosting_distribution_settings_form'),
      'access arguments' => array('administer distribution settings'),
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -1,
    );

    foreach (self::getApiVersions() as $api_version) {
      $items['admin/hosting/distributions/' . $api_version] = array(
        'title' => t('!api_version distributions', ['!api_version' => $api_version]),
        'description' => t('Select which :api_version distributions to support.', [':api_version' => $api_version]),
        'page arguments' => ['hosting_distribution_stock_support_form', $api_version],
        'access arguments' => array('administer distribution settings'),
        'tab_parent' => 'admin/hosting/distributions',
        'type' => MENU_LOCAL_TASK,
      );
    }

    return $items;
  }

  public function getSupportForm() {
    $this->addDistroTable();
    $this->addRefreshButton();
    $this->addSubmitButton('Save configuration');
    return $this->form;
  }

  public static function getApiVersions() {
    return array_keys(self::$distro_version_taxonomy);
  }

  protected function addDistroTable() {
    $header = [
      'title' => t('Name'),
      'version' => t('Latest release'),
      'date' => t('Released'),
      'maintenance_status' => t('Maintenance status'),
      'security_status' => t('Security status'),
      'project_status' => t('Project status'),
    ];

    $options = [];
    foreach ($this->getDistroList() as $name => $title) {
      $distro = new HostingDistributionStockInfo($name, $this->api_version);
      $release = $distro->getReleaseInfo();
      if ($this->releasePassesFilters($release)) {
        $ago = format_interval((time() - $release['date']) , 2) . t(' ago');
        $options[$name] = [
          'title' => [
            'data' => [
              '#type' => 'link',
              '#title' => $title,
              '#href' => $release['link'],
            ],
          ],
          'version' => [
            'data' => [
              '#type' => 'link',
              '#title' => $release['version'],
              '#href' => $release['release_link'],
            ],
          ],
          'date' => $ago,
          'maintenance_status' => $release['maintenance_status'],
          'security_status' => $release['security_status'],
          'project_status' => $release['project_status'],
        ];
      }
    }

    $this->form['supported_distributions_' . $this->api_version[0]] = array(
      '#type' => 'tableselect',
      '#multiple' => TRUE,
      '#header' => $header,
      '#options' => $options,
      '#default_value' => $this->getSupportedStockDistributionsByApiVersion($this->api_version),
      '#empty' => t('No stock distributions available.'),
    );

  }

  protected function getSupportedStockDistributions() {
    $options = [];
    foreach (array_keys(self::$distro_version_taxonomy) as $api_version) {
      $options = array_merge($options, $this->getSupportedStockDistributionsByApiVersion($api_version));
    }
    return $options;
  }

  protected function getSupportedStockDistributionsByApiVersion($api_version) {
    return variable_get('supported_distributions_' . $api_version[0], []);
  }

  protected function releasePassesFilters($release) {
    $passes = TRUE;
    foreach (['security', 'maintenance', 'project'] as $type) {
      if ($passes && $this->filterByStatusType($type)) {
        $passes = $this->releasePassesFilter($release, $type);
      }
    }
    return $passes;
  }

  protected function getStatusOptionsByType($type) {
    $type_statuses = $type . '_statuses';
    return $this->$type_statuses;
  }

  protected function getStatusCriteria($type) {
    $statuses = array_filter($this->getStatuses($type));
    $options = $this->getStatusOptionsByType($type);
    foreach ($statuses as $status) {
      $statuses[$status] = $options[$status];
    }
    return $statuses;
  }

  protected function releasePassesFilter($release, $type) {
    if (in_array($release[$type . '_status'], $this->getStatusCriteria($type))) {
      return TRUE;
    }
    return FALSE;
  }

  protected function getDistroUrl() {
    return format_string($this->distro_list_url, [
      '!status' => $this->project_status,
      '!api_version' => self::$distro_version_taxonomy[$this->api_version],
    ]);
  }

  protected function getDistroList() {
    $distros = &drupal_static(__FUNCTION__, array());
    if (!isset($distros[$this->api_version]) || empty($distros)) {
      $distros = $this->getCachedDistroList();
    }
    // Allow filtering of or additions to the distro list.
    drupal_alter('hosting_stock_distribution_list', $distros[$this->api_version], $this->api_version);
    return $distros[$this->api_version];
  }

  public static function clearCachedDistroList() {
    cache_clear_all(self::CACHE_ID, 'cache');
  }

  protected function getCachedDistroList() {
    $cache = cache_get(self::CACHE_ID);
    if (!$cache || empty($cache->data)) {
      $distros = $this->updateDistroList();
      return $distros;
    }
    return $cache->data;
  }

  protected function updateDistroList() {
    $distros = $this->fetchDistroList();
    cache_set(self::CACHE_ID, $distros);
    return $distros;
  }

  protected function fetchDistroList() {
    $distros = [];
    $original_api_version = $this->api_version;
    foreach ($this->getApiVersions() as $api_version) {
      $this->api_version = $api_version;
      $dom = new DOMDocument();
      $dom->loadHTMLFile($this->getDistroUrl($this->api_version));
      $x = new DOMXPath($dom);
      $distros[$this->api_version] = [];
      foreach($x->query("//div[contains(@class, 'view-project-index')]//ol/li//a") as $node) {
        $machine_name = substr($node->getAttribute('href'), strlen('/project/'));
        $title = $node->textContent;
        $distros[$this->api_version][$machine_name] = $title;
      }
    }
    $this->api_version = $original_api_version;
    return $distros;
  }

  protected function addRefreshButton() {
    $this->form['actions']['refresh'] = array(
      '#type' => 'submit',
      '#value' => t('Refresh distributions'),
      '#submit' => array('hosting_distribution_stock_support_form_submit'),
    );
    $this->form['#api_version'] = $this->api_version;
  }

  protected function addSubmitButton($label = 'Save') {
    $this->form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => t($label),
      '#weight' => -1,
      '#submit' => array('hosting_distribution_stock_support_form_submit'),
    );
    $this->form['#api_version'] = $this->api_version;
  }


  public function submitSupportForm() {
    $this->api_version = $this->form['#api_version'];
    switch ($this->form_state['input']['op']) {
      case 'Refresh distributions':
        self::clearCachedDistroList();
        HostingDistributionStockInfo::clearCachedReleaseInfo($this->api_version);
        break;
      case 'Save configuration':
        $this->saveDistroList();
        break;
    }
  }

  protected function saveDistroList() {
    $distros = 'supported_distributions_' . $this->api_version[0];
    variable_set($distros, $this->form_state['values'][$distros]);
  }

  public function validateSettingsForm() {}

}
