<?php
/**
 * @file The HostingDistributionStockInfo class
 */

class HostingDistributionStockInfo {

  const CACHE_ID = 'hosting_distribution_stock_release_info';

  protected $name = FALSE;
  protected $api_version = FALSE;
  protected $release = FALSE;
  protected $info = [
    'name',
    'version',
    'tag',
    'version_major',
    'version_patch',
    'status',
    'release_link',
    'download_link',
    'date',
    'mdhash',
    'filesize',
    'files',
    'security',
  ];

  public function __construct($name, $api_version) {
    $this->name = $name;
    $this->api_version = $api_version;
    $this->loadReleaseInfo();
  }

  public function getReleaseInfo() {
    return $this->release;
  }

  public static function clearAllCachedReleaseInfo() {
    cache_clear_all(self::CACHE_ID, 'cache');
  }

  public static function clearCachedReleaseInfo($api_version) {
    $cache = cache_get(self::CACHE_ID);
    $cached_data = $cache ? $cache->data : [];
    if (isset($cached_data[$api_version])) {
      unset($cached_data[$api_version]);
      cache_set(self::CACHE_ID, $cached_data);
    }
  }

  protected function loadReleaseInfo($refresh = FALSE) {
    if ($refresh) {
      $this->updateReleaseInfo();
    }
    else {
      $this->release = $this->getCachedReleaseInfo();
    }
  }

  protected function updateReleaseInfo() {
    $this->release = $this->fetchReleaseInfo();
    $this->cacheReleaseInfo();
  }

  protected function getCachedReleaseInfo() {
    $info = &drupal_static(__FUNCTION__, array());
    if (empty($info) || !isset($info[$this->api_version][$this->name])) {
      $cache = cache_get(self::CACHE_ID);
      if (!$cache || !isset($cache->data[$this->api_version][$this->name])) {
        $this->updateReleaseInfo();
        $info[$this->api_version][$this->name] = $this->release;
      }
      else {
        $info = $cache->data;
      }
    }
    return $info[$this->api_version][$this->name];
  }

  protected function cacheReleaseInfo() {
    $cache = cache_get(self::CACHE_ID);
    $cached_data = $cache ? $cache->data : [];
    $cached_data[$this->api_version][$this->name] = $this->release;
    cache_set(self::CACHE_ID, $cached_data);
  }

  protected function fetchReleaseInfo() {
    $xml = file_get_contents($this->getReleaseURL());
    $parser = new SimpleXMLElement($xml);
    $release_info = [];
    foreach ($this->info as $info) {
      if (isset($parser->releases->release->$info)) {
        $release_info[$info] = (string) $parser->releases->release->$info;
      }
    }
    if (empty($release_info['security']) && is_object($parser->releases->release->security)) {
      foreach ($parser->releases->release->security->attributes() as $attribute => $value) {
        if ($attribute == 'covered' && (bool) $value) {
          $release_info['security'] = 'Covered';
        }
      }
    }
    $release_info['link'] = (string) $parser->link;
    $release_info['project_status'] = (string) $parser->project_status;
    $release_info['security_status'] = $release_info['security'];
    unset($release_info['security']);
    foreach ($parser->terms->children() as $term) {
      if ($term->name == 'Maintenance status') {
        $release_info['maintenance_status'] = (string) $term->value;
      }
    }
    return $release_info;
  }

  protected function getReleaseURL() {
    return $this->getReleaseBaseURL() . $this->name. '/' . $this->api_version;
  }

  protected function getReleaseBaseURL() {
    return 'https://updates.drupal.org/release-history/';
  }

}
