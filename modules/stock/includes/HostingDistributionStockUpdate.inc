<?php
/**
 * @file The HostingDistributionStockUpdate class
 */

use Symfony\Component\Yaml\Yaml;

class HostingDistributionStockUpdate extends HostingDistributionMakefileUpdate {

  // A label for this type of update.
  protected $label = 'Stock';

  protected $machine_name = 'stock';

  // Fields that should be formatted in the Aegir style on distribution nodes.
  protected $info_fields = array('field_stock_distribution', 'field_drush_makefile');

  // A description of this type of update.
  protected $description = 'Stock drupal.org distribution updates';

  // An array of data representing a release.
  private $release = array();

  // A generated makefile from which we build a platform.
  private $makefile = FALSE;

  public function __construct($distribution = FALSE) {
    parent::__construct($distribution);
    if ($distribution) {
      $this->initializeRelease();
    }
  }

  protected function getStockDistribution() {
    $distro_array = $this->getStockDistributionArray();
    return $distro_array['distribution'];
  }

  protected function getStockDistributionArray() {
    $raw = $this->getStockDistributionField();
    $raw_array = explode('[', trim($raw, ']'));
    return array(
      'distribution' => $raw_array[0],
      'api_version' => $raw_array[1],
    );
  }

  protected function getStockDistributionField() {
    return $this->distribution->getFieldValue('field_stock_distribution');
  }

  protected function getAPIVersion() {
    $distro_array = $this->getStockDistributionArray();
    return $distro_array['api_version'];
  }

  public function update() {

    $fetch_msg = array(':distribution' => $this->getStockDistribution());
    $this->log->info(t('Fetching release info for :distribution.', $fetch_msg));
    if ($this->fetchReleaseInfo()) {
      $this->log->success(t('Fetched release info for :distribution.', $fetch_msg));
    }
    else {
      $this->log->error(t('Failed to fetch release info for :distribution.', $fetch_msg));
    }
    foreach ($this->release as $key => $value) {
      $this->log->debug("$$key: " . $value);
    }

    $this->log->info(t('Generating makefile contents.'));
    if ($makefile = $this->getMakefileContents()) {
      $this->log->success(t('Generated makefile contents.'));
    }
    else {
      $this->log->error(t('Failed to generate makefile contents.'));
    }
    $this->log->debug('$makefile: ' . $makefile);

    $this->log->info(t('Writing makefile.'));
    if ($makefile_path = $this->writeMakefile($makefile)) {
      $this->log->success(t('Created makefile at :path.', array(':path' => $makefile_path)));
    }
    else {
      $this->log->error(t('Failed to create makefile.'));
    }
    $this->log->debug('$makefile_path: ' . $makefile_path);

    $this->distribution->setFieldValue('field_drush_makefile', $makefile_path);

    parent::update();

    return TRUE;
  }

  private function initializeRelease() {
    $this->release = array(
      'api_version' => $this->getAPIVersion(),
      'distribution' => $this->getStockDistribution(),
    );
  }

  private function getReleaseBaseURL() {
    return 'https://updates.drupal.org/release-history/';
  }

  private function getReleaseURL() {
    return $this->getReleaseBaseURL() . $this->release['distribution'] . '/' . $this->release['api_version'];
  }

  private function fetchReleaseInfo() {
    $this->initializeRelease();
    $xml = file_get_contents($this->getReleaseURL());
    $parser = new SimpleXMLElement($xml);
    foreach (['version', 'mdhash', 'security'] as $info) {
      if (isset($parser->releases->release->$info)) {
        $this->release[$info] = (string) $parser->releases->release->$info;
      }
      else {
        return FALSE;
      }
    }
    return TRUE;
  }

  private function getMakefileContents() {
    $makefile = array(
      'core' => $this->release['api_version'],
      'api' => 2,
      'projects' => array(
        $this->release['distribution'] => array(
          'type' => 'core',
          'version' => $this->release['version'],
        ),
      ),
    );
    return Yaml::dump($makefile, 10, 2);
  }

  /**
   * A validate callback to ensure the Drush makefile is accessible.
   */
  public function validate(&$form, &$form_state) {
  }

  private function writeMakefile($makefile) {
    $temp_file = $this->createMakefile();
    if (file_put_contents($temp_file, $makefile)) {
      return $temp_file;
    }
    else {
      return FALSE;
    }
  }

  private function createMakefile() {
    $temp_dir = file_directory_temp();
    $temp_file = drupal_tempnam($temp_dir, 'stock_' . $this->release['distribution'] . '_');
    chmod($temp_file, 0660);
    return $temp_file;
  }

}
