<?php
/**
 * @file
 * hosting_distribution.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function hosting_distribution_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function hosting_distribution_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function hosting_distribution_node_info() {
  $items = array(
    'distribution' => array(
      'name' => t('Distribution'),
      'base' => 'node_content',
      'description' => t('<strong>A content type that defines a type (or flavor) of platform.</strong> By specifying a target platform, `upgrade` site and platform tasks become available, greatly simplifying maintenance.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
