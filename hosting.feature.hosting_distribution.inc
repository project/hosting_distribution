<?php
/**
 * @file
 * Expose the distribution features to hostmaster.
 */

/**
 * Implements hook_hosting_feature().
 */
function hosting_distribution_hosting_feature() {
  $features = [];

  $features['distribution'] = [
    'title' => t('Distributions'),
    'description' => t('Group platforms into "distributions" to simplify updates.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_distribution',
    'role_permissions' => [
      'aegir client' => [
        'create site upgrade task',
      ],
      'aegir platform manager' => [
        'view distribution list',
        'create distribution content',
        'edit own distribution content',
        'edit any distribution content',
        'delete own distribution content',
        'delete any distribution content',
        'create platform upgrade task',
      ],
      'aegir administrator' => [
        'administer distribution settings',
      ],
    ],
    'group' => 'experimental',
  ];

  $features['distribution_makefile'] = [
    'title' => t('Distributions (Drush Make support)'),
    'description' => t('Automate distribution platform builds based on Drush makefiles.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_distribution_makefile',
    'group' => 'experimental',
  ];

  $features['distribution_stock'] = [
    'title' => t('Distributions (Stock distro support)'),
    'description' => t('Automate distribution platform builds based on drupal.org stock distributions.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_distribution_stock',
    'group' => 'experimental',
  ];

  $features['distribution_git'] = [
    'title' => t('Distributions (Git support)'),
    'description' => t('Automate distribution platform builds based on Git.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_distribution_git',
    'group' => 'experimental',
  ];

  return $features;
}
